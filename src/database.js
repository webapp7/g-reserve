import sqlite3 from 'sqlite3';

const DBNAME = 'sqlite3.db';

export default class Database {
  constructor() {
    this.db = new sqlite3.Database(DBNAME);
  }

  initializeDB() {
    const tables = [
      `slots (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        year INTEGER NOT NULL,
        month INTEGER NOT NULL,
        day INTEGER NOT NULL,
        start INTEGER NOT NULL,
        end INTEGER NOT NULL
      )`,
    ];
    const tableInitializer = tables.map((table) => this.createTable(table));
    return Promise.all(tableInitializer);
  }

  createTable(table) {
    return new Promise((resolve, reject) => {
      this.db.run(`CREATE TABLE IF NOT EXISTS ${table}`, (err) => {
        if (err) {
          reject(err);
          return;
        }
        resolve();
      });
    });
  }

  getAllSlots() {
    return new Promise((resolve, reject) => {
      this.db.all('SELECT * FROM slots', (err, rows) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(rows);
      });
    });
  }

  close() {
    this.db.close();
  }
}
