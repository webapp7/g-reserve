import path from 'path';
import express from 'express';
import Database from './database';

const app = express();
app.use(express.json());
app.use(express.static('public'));

const conn = new Database();

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.get('/slots', (req, res) => {
  (async () => {
    const slots = await conn.getAllSlots();
    res.json(slots);
  })();
});

(async () => {
  await conn.initializeDB();
  app.listen(4000);
})();
