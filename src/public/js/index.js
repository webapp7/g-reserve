import axios from 'axios';
import $ from 'jquery';

$(() => {
  axios.get('slots')
    .then((response) => {
      const $slots = $('#slots');
      $slots.val(JSON.stringify(response.data, null, 2));
    });
});
