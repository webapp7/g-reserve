const path = require('path');

module.exports = {
  entry: './src/public/js/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist/public/js'),
  },
  mode: 'development',
};
